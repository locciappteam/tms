﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMS.Core.Data;
using TMS.Core.Entidades.Fatura;
using TMS.Core.Utilitarios;
using TMS.Core.WebAPI;

namespace TMS.App
{
    class Program
    {
        static void Main(string[] args)
        {
            Uri baseAddress;
            string Auth;

            using (DataContext _ctx = new DataContext())
            {
                _ctx.Database.CommandTimeout = 300;

                List<TMS_PARAMETROS> EcParametros = _ctx.Set<TMS_PARAMETROS>().ToList();

                string User = EcParametros.Where(a => a.PARAMETRO == "USUARIO_API").Select(a => a.VALOR).First();
                string Pass = EcParametros.Where(a => a.PARAMETRO == "SENHA_API").Select(a => a.VALOR).First();
                baseAddress = new Uri(EcParametros.Where(a => a.PARAMETRO == "BASE_ADDRESS").Select(a => a.VALOR).First());

                Auth = Convert.ToBase64String(Encoding.ASCII.GetBytes(String.Format($"{User}:{Pass}")));
            }

           ImportaPedidos.Cegid(DateTime.Today.AddDays(-30), new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddDays(-1));

            Task.Run(async () =>
            {
                await apiFaturaPost.ConfirmaFaturamento(baseAddress, Auth);
            }).Wait();
       }
    }
}

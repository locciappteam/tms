﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Reflection;
using TMS.Core.Entidades.Fatura;
using TMS.Core.Entidades.Universal;
using TMS.Core.Entidades.Views;

namespace TMS.Core.Data
{
    public class DataContext:DbContext
    {
        public DataContext()
            :base("Data Source=AMSAOCBRDB01;Initial Catalog=PORTAL_LOCCITANE;Persist Security Info=True;User ID=portalloccitane;Password=3u3NO483")
        {}

        public DbSet<ESTADO> Estado { get; set; }
        public DbSet<ESTABELECIMENTO> Estabelecimento { get; set; }

        public DbSet<TMS_PARAMETROS> TmsParametros { get; set; }
        public DbSet<TMS_FATURA_CONTROLE> TmsFaturaControle { get; set; }
        public DbSet<TMS_FATURA_RETORNO> TmsFaturaRetorno { get; set; }

        public DbSet<W_TMS_PEDIDO_FATURADO> wTmsPedidoFaturado { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
        }
    }
}

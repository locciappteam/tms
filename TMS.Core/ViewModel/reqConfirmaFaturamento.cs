﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS.Core.ViewModel
{
    public class reqConfirmaFaturamento
    {
        public string ped_cd_empresa { get; set; }
        public string ped_cd_cliente { get; set; }

        [JsonIgnore]
        public string ped_nu_pedido_origem { get; set; }
        [JsonProperty("ped_nu_pedido_origem")]
        public string ped_nu_pedido_origem_str
        {
            get
            {
                return ped_nu_pedido_origem.Replace("BR0001-", "");
            }
            set
            {
                ped_nu_pedido_origem = ped_nu_pedido_origem.Replace("BR0001-", "");
            }
        }

        public int? ped_nu_nf { get; set; }
        public string ped_nu_serie_nf { get; set; }

        [JsonIgnore]
        public DateTime? ped_dt_emissao_nf { get; set; }
        [JsonProperty("ped_dt_emissao_nf")]
        public string ped_dt_emissao_nf_str
        {
            get
            {
                return ped_dt_emissao_nf != null ? ped_dt_emissao_nf.Value.ToString("dd/MM/yyyy hh:mm:ss") : null;
            }
            set
            {
                ped_dt_emissao_nf = string.IsNullOrEmpty(value) ? default(DateTime?) : DateTime.ParseExact(value, "dd/MM/yyyy hh:mm:ss", null);
            }
        }
        public string ped_chave_acesso { get; set; }
        public int? ped_qt_volume { get; set; }
        public int ped_cfop { get; set; }

        public decimal ped_vlr_nf { get; set; }
        public string emailCliente { get; set; }
        public string canalVenda { get; set; }
        public string cnpjTransportadora { get; set; }
    }
}

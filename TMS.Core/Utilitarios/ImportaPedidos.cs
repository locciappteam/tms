﻿using System;
using System.Data.SqlClient;
using TMS.Core.Data;

namespace TMS.Core.Utilitarios
{
    public class ImportaPedidos
    {
        public static void Cegid(DateTime DataInicial, DateTime DataFinal)
        {
            Log.Write(String.Format($"{DateTime.Now} : Cegid - Importando notas faturadas"));

            using (DataContext _ctx = new DataContext())
            {
                _ctx.Database.CommandTimeout = 300;

              var x =  _ctx.Database.ExecuteSqlCommand(
                    
                    "dbo.usp_ATUALIZA_TMS_FATURA_CONTROLE {0}, {1}",
                        new SqlParameter("@DTINICIAL", System.Data.SqlDbType.DateTime).Value = DataInicial,
                        new SqlParameter("@DTFINAL", System.Data.SqlDbType.Date).Value = DataFinal
                    );

                /*
                List<TMS_FATURA_CONTROLE> TmsPedidoFaturado =
                (
                    from
                        a in _ctx.wTmsPedidoFaturado.ToList().Where(a=> a.DATA_EMISSAO >= DataInicial && a.DATA_EMISSAO <= DataFinal)
                    
                    select new TMS_FATURA_CONTROLE
                    {
                        ESTABELECIMENTOID = a.ESTABELECIMENTOID,
                        NUM_PEDIDO = a.NUM_PEDIDO,
                        NUMERO_NF = a.NUMERO_NF,
                        SERIE_NF = a.SERIE_NF,
                        DATA_EMISSAO = a.DATA_EMISSAO,
                        CHAVE_NF = a.CHAVE_NF,
                        COD_CLIENTE = a.COD_CLIENTE,
                        EMAIL_CLIENTE = a.EMAIL_CLIENTE,
                        QTDE_VOLUME = a.QTDE_VOLUME,
                        CFOP_PREDOMINANTE = a.CFOP_PREDOMINANTE,
                        VLR_NOTA = a.VLR_NOTA,
                        CNPJ_TRANSPORTADORA = a.CNPJ_TRANSPORTADORA,
                        CANAL_VENDA = a.CANAL_VENDA,
                        CADASTRADO_POR = "INTEGRACAO",
                        DATA_CADASTRO = DateTime.Now,
                        ALTERADO_POR = "INTEGRACAO",
                        DATA_ALTERACAO = DateTime.Now
                    }
                ).ToList();

                _ctx.Set<TMS_FATURA_CONTROLE>().AddRange(TmsPedidoFaturado);
                _ctx.SaveChanges();*/

            }                
        }
    }
}

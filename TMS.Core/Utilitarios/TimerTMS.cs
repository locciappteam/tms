﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using TMS.Core.Data;
using TMS.Core.Entidades.Fatura;
using TMS.Core.WebAPI;

namespace TMS.Core.Utilitarios
{
    public class TimerTMS
    {
        static Timer timer;

        public static void TimeScheduler()
        {
            try
            {
                Log.Write(strMensagem: String.Format($"{DateTime.Now} : Realizando agendamento..."), Tipo: 0);

                DateTime ScheduledTime = DateTime.Now.AddMinutes(10);

                Log.Write(String.Format($"{DateTime.Now} : Tarefa agendada para {ScheduledTime}"));

                double TickTime = (double)(ScheduledTime - DateTime.Now).TotalMilliseconds;
                timer = new Timer(TickTime);
                timer.Elapsed += new ElapsedEventHandler(TimeElapsed);
                timer.Start();
            }
            catch (Exception ex)
            {
                Log.Write(String.Format($"{DateTime.Now} : Erro Fatal: {ex.ToString()}"));
                Email.Enviar("am03svc-cegid@loccitane.com.br", "dlbr-itapplications@loccitane.com", ex.ToString());
            }
        }

        private static void TimeElapsed(object sender, ElapsedEventArgs e)
        {
            timer.Stop();
            Log.Write(String.Format($"{DateTime.Now} : Iniciando a tarefa..."));

            try
            {
                Uri baseAddress;
                string Auth;

                using (DataContext _ctx = new DataContext())
                {
                    List<TMS_PARAMETROS> EcParametros = _ctx.Set<TMS_PARAMETROS>().ToList();

                    string User = EcParametros.Where(a => a.PARAMETRO == "USUARIO_API").Select(a => a.VALOR).First();
                    string Pass = EcParametros.Where(a => a.PARAMETRO == "SENHA_API").Select(a => a.VALOR).First();
                    baseAddress = new Uri(EcParametros.Where(a => a.PARAMETRO == "BASE_ADDRESS").Select(a => a.VALOR).First());

                    Auth = Convert.ToBase64String(Encoding.ASCII.GetBytes(String.Format($"{User}:{Pass}")));
                }

                ImportaPedidos.Cegid(DateTime.Today.AddDays(-5), new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddDays(-1));

                Task.Run(async () =>
                {
                    await apiFaturaPost.ConfirmaFaturamento(baseAddress, Auth);
                }).Wait();
            }
            catch (Exception ex)
            {
                Log.Write(String.Format($"{DateTime.Now} : \n ========== ERRO FATAL ========== \n {ex.ToString()}"));
                Email.Enviar("am03svc-cegid@loccitane.com.br", "diego.bueno@loccitane.com", ex.ToString());
            }

            Log.Write(String.Format($"{DateTime.Now} : Fim da tarefa..."));
            TimeScheduler();
        }
    }
}

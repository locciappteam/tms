﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TMS.Core.Data;
using TMS.Core.Entidades.Fatura;
using TMS.Core.Utilitarios;
using TMS.Core.ViewModel;

namespace TMS.Core.WebAPI
{
    public class apiFaturaPost
    {
        public static async Task ConfirmaFaturamento(Uri baseAddress, string Auth)
        {
            Log.Write(String.Format($"{DateTime.Now} : WebAPI - Enviando notas faturadas"));
            List<reqConfirmaFaturamento> reqconfirmafaturamento = new List<reqConfirmaFaturamento>();
            using (DataContext _ctx = new DataContext())
            {
                _ctx.Database.CommandTimeout = 300;

                reqconfirmafaturamento = (
                     from
                         fatura in _ctx.TmsFaturaControle
                     from
                         retorno in _ctx.TmsFaturaRetorno.Where(retorno => retorno.NUM_PEDIDO == fatura.NUM_PEDIDO).DefaultIfEmpty()
                     where
                         retorno.DESC_RETORNO == null & fatura.CHAVE_NF != ""

                     select new reqConfirmaFaturamento
                     {
                         ped_cd_empresa = fatura.ESTABELECIMENTO.EMPRESA_SAP,
                         ped_cd_cliente = null,
                         ped_nu_pedido_origem = fatura.NUM_PEDIDO,
                         ped_nu_nf = fatura.NUMERO_NF,
                         ped_nu_serie_nf = fatura.SERIE_NF,
                         ped_dt_emissao_nf = fatura.DATA_EMISSAO,
                         ped_chave_acesso = fatura.CHAVE_NF,
                         ped_qt_volume = fatura.QTDE_VOLUME,
                         ped_cfop = fatura.CFOP_PREDOMINANTE,
                         ped_vlr_nf = fatura.VLR_NOTA,
                         emailCliente = fatura.EMAIL_CLIENTE,
                         canalVenda = fatura.CANAL_VENDA,
                         cnpjTransportadora = fatura.CNPJ_TRANSPORTADORA
                     }
                 ).ToList();
            }
            string JsonFatura = JsonConvert.SerializeObject(reqconfirmafaturamento);

            if (reqconfirmafaturamento.Any())
            {
                using (var httpClient = new HttpClient { BaseAddress = baseAddress })
                {
                    httpClient.Timeout = TimeSpan.FromMinutes(15);
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Auth);

                    using (var content = new StringContent(JsonFatura, System.Text.Encoding.Default, "application/json"))
                    {
                        using (var response = await httpClient.PostAsync("entrada/faturamentonfe", content))
                        {
                            string responseData = await response.Content.ReadAsStringAsync();

                            if (response.IsSuccessStatusCode)
                            {
                                foreach (var Retorno in reqconfirmafaturamento)
                                {
                                    TMS_FATURA_RETORNO TmsFaturaRetorno = new TMS_FATURA_RETORNO
                                    {
                                        NUM_PEDIDO = Retorno.ped_nu_pedido_origem,
                                        CHAVE_NF = Retorno.ped_chave_acesso,
                                        STATUS_RETORNO = (int)response.StatusCode,
                                        DESC_RETORNO = responseData.ToString(),
                                        CADASTRADO_POR = "INTEGRACAO",
                                        DATA_CADASTRO = DateTime.Now,
                                        ALTERADO_POR = "INTEGRACAO",
                                        DATA_ALTERACAO = DateTime.Now
                                    };

                                    using (DataContext _ctx = new DataContext())
                                    {
                                        _ctx.Set<TMS_FATURA_RETORNO>().Add(TmsFaturaRetorno);
                                        _ctx.SaveChanges();
                                    }
                                }
                            }
                            else
                            {
                                Log.Write(String.Format($"{DateTime.Now} : \n ========== ERRO API ========== \n {response.StatusCode.ToString()} - {response.RequestMessage.ToString()}"));
                                Email.Enviar("am03svc-cegid@loccitane.com.br", "diego.bueno@loccitane.com", $"Erro na requisição da WebApi - {response.StatusCode.ToString()} - {response.RequestMessage.ToString()}");

                            }
                        }
                    }
                } 
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMS.Core.Entidades.Views
{
    public class W_TMS_PEDIDO_FATURADO
    {
        public int ESTABELECIMENTOID { get; set; }

        public string NUM_PEDIDO { get; set; }
        public int NUMERO_NF { get; set; }
        public string SERIE_NF { get; set; }
        public DateTime DATA_EMISSAO { get; set; }
        [Key]
        public string CHAVE_NF { get; set; }
        public string COD_CLIENTE { get; set; }
        public string EMAIL_CLIENTE { get; set; }
        public int QTDE_VOLUME { get; set; }
        public int CFOP_PREDOMINANTE { get; set; }
        public decimal VLR_NOTA { get; set; }
        public string CNPJ_TRANSPORTADORA { get; set; }
        public string CANAL_VENDA { get; set; }
    }
}

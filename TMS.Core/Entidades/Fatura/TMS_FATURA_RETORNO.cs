﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMS.Core.Entidades.Fatura
{
    public class TMS_FATURA_RETORNO
    {
        public TMS_FATURA_RETORNO()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string NUM_PEDIDO { get; set; }
        [ForeignKey("TMS_FATURA_CONTROLE")]
        public string CHAVE_NF { get; set; }
        public int STATUS_RETORNO { get; set; }
        public string DESC_RETORNO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual TMS_FATURA_CONTROLE TMS_FATURA_CONTROLE { get; set; }
    }
}

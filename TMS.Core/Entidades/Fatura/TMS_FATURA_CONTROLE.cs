﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TMS.Core.Entidades.Universal;

namespace TMS.Core.Entidades.Fatura
{
    public class TMS_FATURA_CONTROLE
    {
        public TMS_FATURA_CONTROLE()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [ForeignKey("ESTABELECIMENTO")]
        public int ESTABELECIMENTOID { get; set; }
        public string NUM_PEDIDO { get; set; }
        [Key]
        public string CHAVE_NF { get; set; }
        public int NUMERO_NF { get; set; }
        public string SERIE_NF { get; set; }
        public DateTime DATA_EMISSAO { get; set; }
        public string COD_CLIENTE { get; set; }
        public string EMAIL_CLIENTE { get; set; }
        public int QTDE_VOLUME { get; set; }
        public int CFOP_PREDOMINANTE { get; set; }
        public decimal VLR_NOTA { get; set; }
        public string CNPJ_TRANSPORTADORA { get; set; }
        public string CANAL_VENDA { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual ESTABELECIMENTO ESTABELECIMENTO { get; set; }
        public virtual ICollection<TMS_FATURA_RETORNO> TMS_FATURA_RETORNOs { get; set; }
    }
}

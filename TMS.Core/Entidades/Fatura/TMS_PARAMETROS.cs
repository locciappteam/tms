﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TMS.Core.Entidades.Fatura
{
    public class TMS_PARAMETROS
    {
        public TMS_PARAMETROS()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        public string PARAMETRO { get; set; }
        public string DESC_PARAMETRO { get; set; }
        public string VALOR { get; set; }
        public string TIPO { get; set; }
        public bool ATIVO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }
    }
}

﻿namespace TMS.WinService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.serviceProcessInstallerTMS = new System.ServiceProcess.ServiceProcessInstaller();
            this.serviceInstallerTMS = new System.ServiceProcess.ServiceInstaller();
            // 
            // serviceProcessInstallerTMS
            // 
            this.serviceProcessInstallerTMS.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.serviceProcessInstallerTMS.Password = null;
            this.serviceProcessInstallerTMS.Username = null;
            // 
            // serviceInstallerTMS
            // 
            this.serviceInstallerTMS.Description = "Envia chamada WebAPI de pedidos faturados";
            this.serviceInstallerTMS.DisplayName = "LOcc - TMS";
            this.serviceInstallerTMS.ServiceName = "ServiceTMS";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.serviceProcessInstallerTMS,
            this.serviceInstallerTMS});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstallerTMS;
        private System.ServiceProcess.ServiceInstaller serviceInstallerTMS;
    }
}
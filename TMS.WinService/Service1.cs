﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using TMS.Core.Utilitarios;

namespace TMS.WinService
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Log.Write(strMensagem: String.Format($"{DateTime.Now} : Serviço Iniciado!"));
            TimerTMS.TimeScheduler();
        }

        protected override void OnStop()
        {
            Log.Write(String.Format($"{DateTime.Now} : Serviço Paralisado!"));
        }
    }
}
